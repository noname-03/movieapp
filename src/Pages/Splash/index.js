import { StyleSheet, Text, View,Image } from 'react-native'
import React, {useEffect} from 'react'
import { LogoApp } from "../../assets";

const Splash = ({ navigation }) => {
    useEffect(() => {
        setTimeout(() => {
            navigation.replace('Home')
        }, 3000)
    }, [navigation])
    return (
        <View style={styles.background}>
            <Image source={LogoApp} style={styles.image} />
            <Text style={styles.text}>WEFLIX</Text>
            <Text style={styles.text2}>FAHRURROZI - UCIC</Text>
        </View>
    )

}

export default Splash

const styles = StyleSheet.create({
    background: {
        flex : 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    text: {
        top: 20,
        justifyContent: 'center',
        color: 'black',
        fontSize: 20,
        fontWeight: 'bold'
    },
    text2: {
        top: 300,
        color: 'black'
        // fontSize: 20
    },
    image: {
        justifyContent: 'center',
        alignItems: 'center',
        width: 359,
        height: 100
    }

})