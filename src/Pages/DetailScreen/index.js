import axios from 'axios';
import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
  ImageBackground,
  ActivityIndicator,
  Dimensions,
  TouchableOpacity,
  Alert,
} from 'react-native';
import Box from '../../components/Box/Box';
// import {Back, Like, Share} from '../../assets/icon';

const DetailScreen = ({route, navigation}) => {
  const {id} = route.params;
  const [detailMovie, setdetailMovie] = useState([]);
  const [Cast, setCast] = useState([]);
  const [Genre, setGenre] = useState([]);
  const [Visibility, setVisibility] = useState(true);

  useEffect(() => {
    setTimeout(() => {
      setVisibility(!Visibility);
    }, 1000);
    axios
      .get(`http://code.aldipee.com/api/v1/movies/${id}`)
      .then(responseData => {
        setdetailMovie(responseData.data);
        setCast(responseData.data.credits.cast);
        setGenre(responseData.data.genres);
      })
      .catch(err => {
        Alert.alert('ERROR MESSAGE', `${err}`);
      });
  }, []);
  return (
    <View style={styles.container}>
      {Visibility && (
        <View>
          <ActivityIndicator
            animating={Visibility}
            hidesWhenStopped={Visibility}
            size={100}
          />
        </View>
      )}
      {!Visibility && (
        <ScrollView showsVerticalScrollIndicator={false}>
          <ImageBackground
            source={{uri: `${detailMovie.backdrop_path}`}}
            style={styles.header}>
            <View style={styles.buttonContainer}>
              {/* <TouchableOpacity
                onPress={() => {
                  navigation.navigate('Home');
                }}>
                <Back />
              </TouchableOpacity> */}
              <View style={{flexDirection: 'row'}}>
                {/* <TouchableOpacity
                  onPress={() => {
                    Alert.alert('Anda telah menyukai film ini');
                  }}>
                  <Like />
                </TouchableOpacity> */}
              </View>
            </View>
          </ImageBackground>
          <Box
            image={detailMovie.poster_path}
            title={detailMovie.title}
            tagline={detailMovie.tagline}
            status={detailMovie.status}
            runtime={detailMovie.runtime}
            vote_average={detailMovie.vote_average}
            release_date={detailMovie.release_date}
          />
          <View>
            <View>
              <Text style={styles.menu}>Genres</Text>
              <ScrollView
                horizontal={true}
                showsHorizontalScrollIndicator={false}>
                {Genre.map(item => {
                  return (
                    <View
                      style={{
                        alignSelf: 'center',
                        backgroundColor: '#f194ff',
                        margin: 10,
                        paddingHorizontal: 10,
                        borderRadius: 10,
                      }}>
                      <Text style={styles.genre}>{item.name}</Text>
                    </View>
                  );
                })}
              </ScrollView>
            </View>
            <View>
              <Text style={styles.menu}>Synopsis</Text>
              <Text style={styles.sinopsis}>{detailMovie.overview}</Text>
            </View>

            <View>
              <Text style={styles.menu}>Artist</Text>
              <ScrollView horizontal={true}>
                {Cast.map(item => {
                  return (
                    <View
                      style={{
                        flex: 1,
                        backgroundColor: '#f194ff',
                        alignItems: 'center',
                        justifyContent: 'center',
                        margin: 10,
                        paddingVertical: 10,
                        paddingHorizontal: 10,
                        borderRadius: 10,
                        width: 175,
                      }}>
                      <View style={{flex: 3}}>
                        <Image
                          source={{
                            uri: `${item.profile_path}`,
                          }}
                          style={styles.poster}
                        />
                      </View>
                      <View style={{flex: 1}}>
                        <Text style={styles.castname}>{item.name}</Text>
                      </View>
                    </View>
                  );
                })}
              </ScrollView>
            </View>
          </View>
        </ScrollView>
      )}
    </View>
  );
};

export default DetailScreen;

const screens = Dimensions.get('screen');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'center',
  },
  header: {
    width: screens.width,
    height: screens.height * 0.25,
    alignItems: 'center',
  },
  menu: {
    color: 'black',
    paddingLeft: 10,
    fontSize: 20,
    paddingTop: 10,
    paddingBottom: 5,
    fontWeight: 'bold',
  },
  text: {
    color: 'black',
    fontSize: 12,
  },
  genre: {
    color: 'black',
    fontSize: 12,
  },
  sinopsis: {
    color: 'black',
    fontSize: 12,
    textAlign: 'justify',
    paddingHorizontal: 10,
  },
  poster: {
    width: 150,
    height: 150,
    resizeMode: 'cover',
  },
  castname: {
    color: 'black',
    fontSize: 20,
    textAlign: 'center',
    fontWeight: 'bold'
  },
  button: {
    width: 100,
    height: 100,
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 50,
    width: screens.width * 0.95,
  },
});
