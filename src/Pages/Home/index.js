import {
    StyleSheet, Text, View, Image, ScrollView, Button, Alert,
    Dimensions, TouchableOpacity
} from 'react-native'
import React, { useEffect, useState } from 'react'
import axios from 'axios';


function Home({navigation}) {
    const [movie, setmovie] = useState([]);
    const [Visibility, setVisibility] = useState(true);

    useEffect(() => {
        setTimeout(() => {
            setVisibility(!Visibility);
        }, 1000);
        axios
            .get('http://code.aldipee.com/api/v1/movies')
            .then(responseData => {
                setmovie(responseData.data.results);
            })
            .catch(err => {
                Alert.alert('ERROR MESSAGE', `${err}`);
            });
    }, []);


    return (
        <View style={styles.container}>
            <ScrollView>
                <Text style={styles.text}>Recomended</Text>
                <View style={styles.header}>
                    <ScrollView horizontal={true}>
                        {movie.map(item => {
                            return (
                                <View>
                                    <Image source={{ uri: `${item.poster_path}` }} style={styles.imagehead} />
                                </View>
                            );
                        })}
                    </ScrollView>
                </View>
                <Text style={styles.text} >Latest Upload</Text>
                {movie.map(item => {
                    return (
                        <View style={styles.body}>
                            <Image source={{ uri: `${item.poster_path}` }} style={styles.imagebody} />
                            <View style={styles.text2}>
                                <Text style={styles.texttitle}>{item.title}</Text>
                                <Text style={styles.textdesc}>{item.overview}</Text>
                                <View style={styles.forbutton}>
                                    <Button title="Show More" color="#f194ff" 
                                    onPress={() => {
                                        navigation.navigate('DetailScreen', {
                                            id: `${item.id}`,
                                        });
                                    }} />
                                </View>

                            </View>
                        </View>
                    );
                })}
            </ScrollView>
        </View>
    );
}

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;


export default Home

const styles = StyleSheet.create({
    constainer: {
        flexDirection: 'column'
    },
    text: {
        paddingTop: 16,
        paddingBottom: 12
    },
    header: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    imagehead: {
        marginLeft: 12,
        width: 60,
        height: 84,
        borderRadius: 10
    },
    body: {
        paddingTop: 12,
        flexDirection: 'row',
    },
    imagebody: {
        marginLeft: 12,
        width: windowWidth * 0.25,
        height: windowHeight * 0.14
    },
    text2: {
        marginLeft: 12,
        marginRight: 120
    },
    texttitle: {
        fontWeight: 'bold'
    },
    textdesc: {
        textAlign: 'justify',
        fontSize: 12
    },
    forbutton: {
        width: 120
    },
    button: {
        backgroundColor: 'white'
    }
})
